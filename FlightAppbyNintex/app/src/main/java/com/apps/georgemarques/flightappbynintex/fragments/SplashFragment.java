package com.apps.georgemarques.flightappbynintex.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps.georgemarques.flightappbynintex.R;
import com.apps.georgemarques.flightappbynintex.SearchActivity;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by george on 02/06/15.
 */
public class SplashFragment extends Fragment {
    private Timer timer;
    public static final String TAG = "SplashFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.splash_fragment, container, false);
        timer = new Timer();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof SearchActivity){
            getActivity().getActionBar().hide();
        }

        // Shows SplashScreen for 3 seconds
        timer.schedule(new TimerTask() {
            public void run() {
                callSearchFlightsFragment();
            }
        }, 3000);
    }

    private void callSearchFlightsFragment(){
        SearchFragment searchFragment = new SearchFragment();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragmentParentViewGroup, searchFragment);
        fragmentTransaction.addToBackStack(SearchFragment.TAG);
        fragmentTransaction.commit();



    }
}
