package com.apps.georgemarques.flightappbynintex.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps.georgemarques.flightappbynintex.R;
import com.apps.georgemarques.flightappbynintex.rest.model.FlightSearchData;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by george on 28/05/15.
 */
public class FlightsAdapter extends RecyclerView.Adapter<FlightsViewHolder> {
    private List<FlightSearchData> flightsList;
    public List<FlightSearchData> getFlightsList(){
        return this.flightsList;
    }

    public FlightsAdapter(List<FlightSearchData> list){
        this.flightsList = list;
    }

    @Override
    public int getItemCount(){
        return flightsList.size();
    }

    @Override
    public void onBindViewHolder(FlightsViewHolder flightsViewHolder, int i) {
        FlightSearchData flight = flightsList.get(i);

        flightsViewHolder.inboundDuration.setText(flight.getInboundFlightsDuration());
        flightsViewHolder.outboundDuration.setText(flight.getOutboundFlightsDuration());
        flightsViewHolder.airlineName.setText(flight.getAirlineName());
        flightsViewHolder.totalAMount.setText(String.valueOf(flight.getTotalAmount()));

        // Use of Universal Image Loader lib to download the Image Package asynchronously
        ImageLoader.getInstance().displayImage(flight.getAirlineLogoAddress(),flightsViewHolder.airlineLogo);
    }

    @Override
    public FlightsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.flight_card_view, viewGroup, false);
        return new FlightsViewHolder(itemView);
    }

}
