package com.apps.georgemarques.flightappbynintex.fragments;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.georgemarques.flightappbynintex.R;
import com.apps.georgemarques.flightappbynintex.SearchActivity;
import com.apps.georgemarques.flightappbynintex.model.FlightSearchInfo;
import com.apps.georgemarques.flightappbynintex.util.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by george on 28/05/15.
 */
public class SearchFragment extends Fragment {
    @InjectView(R.id.departureDateButton)
    ImageButton departureDateBtn;

    @InjectView(R.id.returnDateButton)
    ImageButton returnDateBtn;

    @InjectView(R.id.selectedReturnDate)
    TextView selectedReturnDate;

    @InjectView(R.id.selectedDepartureDate)
    TextView selectedDepartureDate;

    @InjectView(R.id.searchButton)
    ImageButton searchButton;

    @InjectView(R.id.departureCodeInput)
    TextView departureCodeInput;

    @InjectView(R.id.arrivalCodeInput)
    TextView arrivalCodeInput;

    private DatePickerDialog departureDatePicker;
    private String departureDate;
    private String returnDate;
    public static final String TAG = "SearchFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.search_fragment, container, false);
        ButterKnife.inject(this, view);
        departureDate = getString(R.string.choose_date);
        returnDate = departureDate;

        selectedReturnDate.setText(returnDate);
        selectedDepartureDate.setText(departureDate);

        departureCodeInput.setOnFocusChangeListener(focusChangeListener);
        arrivalCodeInput.setOnFocusChangeListener(focusChangeListener);
        return view;
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof SearchActivity){
            getActivity().getActionBar().show();
        }
    }

    @OnClick(R.id.departureDateButton)
    public void departureDateClick(){
        Calendar c = Calendar.getInstance();
        departureDatePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                selectedDepartureDate.setText(String.valueOf(dayOfMonth) + "/" +
                                              String.valueOf(monthOfYear)+ "/" +
                                              String.valueOf(year)
                );
                Calendar c = new GregorianCalendar(year, monthOfYear, dayOfMonth,0,0,0);
                Date d = c.getTime();
                DateFormat df = new SimpleDateFormat(Utils.DATE_PATTERN);
                departureDate = df.format(d);

            }
        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));

        departureDatePicker.show();
    }

    @OnClick(R.id.returnDateButton)
    public void returnDateClick(){
        Calendar c = Calendar.getInstance();

        departureDatePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                selectedReturnDate.setText(String.valueOf(dayOfMonth) + "/" +
                                String.valueOf(monthOfYear)+ "/" +
                                String.valueOf(year)
                );
                Calendar c = new GregorianCalendar(year, monthOfYear, dayOfMonth,0,0,0);
                Date d = c.getTime();
                DateFormat df = new SimpleDateFormat(Utils.DATE_PATTERN);
                returnDate = df.format(d);
            }
        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));

        departureDatePicker.show();
    }

    @OnClick(R.id.searchButton)
    public void searchButtonClick() {
        if (hasEmptyFields()) {
            Toast.makeText(getActivity(), R.string.alert_message_empty_field, Toast.LENGTH_SHORT).show();
        } else {
            // Codes Validation
            if (!hasOnlyLetters() || !hasValidCodeLength()){
                Toast.makeText(getActivity(), R.string.alert_invalid_code, Toast.LENGTH_SHORT).show();
            }
            else {
                if (!Utils.isGreaterDate(departureDate, returnDate)) {
                    Toast.makeText(getActivity(), getString(R.string.date_error_message), Toast.LENGTH_SHORT).show();
                } else {

                    if (Utils.hasInternetConnectivity(getActivity())) {
                        callListFlightsFragment();
                    } else {
                        String title = getString(R.string.alert_dialog_title);
                        String message = getString(R.string.internet_alert_message);

                        Utils.showAlertDialog(title, message, getActivity());
                    }
                }
            }
        }
    }

    public void callListFlightsFragment() {
        FragmentManager fm = getActivity().getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        FlightsListFragment listFragment = new FlightsListFragment();

        /*** Prepare Bundle to send data to be requested at ListFlightsFragments          ***/
        FlightSearchInfo flightSearchInfo = new FlightSearchInfo(departureDate, returnDate,
                departureCodeInput.getText().toString(), arrivalCodeInput.getText().toString());
        Bundle arguments = new Bundle();
        arguments.putParcelable(FlightSearchInfo.BUNDLE_KEY, flightSearchInfo);
        listFragment.setArguments(arguments);

        fragmentTransaction.replace(R.id.fragmentParentViewGroup, listFragment);
        fragmentTransaction.addToBackStack(FlightsListFragment.TAG);
        fragmentTransaction.commit();
    }

    private View.OnFocusChangeListener focusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if(!hasFocus){
                hideKeyboard(v);
            }
        }
    };

    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    /** Validations **/

    private boolean hasEmptyFields(){
        String defaultDate = getString(R.string.choose_date);

        if (departureCodeInput.getText().toString().isEmpty() ||
                arrivalCodeInput.getText().toString().isEmpty() ||
                departureDate.equals(defaultDate) ||
                returnDate.equals(defaultDate)){
            return true;
        }
        return false;
    }

    private boolean hasValidCodeLength(){
        if (departureCodeInput.length() != 3 || arrivalCodeInput.length() !=3){
            return false;
        }
        return true;
    }

    private boolean hasOnlyLetters(){
        if (!isAlpha(departureCodeInput.getText().toString())||
                !isAlpha(arrivalCodeInput.getText().toString())){
            return false;
        }
        return true;
    }

    private boolean isAlpha(String name) {
        return name.matches("[a-zA-Z]+");
    }

}
