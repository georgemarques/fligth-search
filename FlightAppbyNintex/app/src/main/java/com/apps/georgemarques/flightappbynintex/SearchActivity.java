package com.apps.georgemarques.flightappbynintex;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.apps.georgemarques.flightappbynintex.fragments.SearchFragment;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;


public class SearchActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        initImageLoader();
        callSearchFragment();
    }

    private void initImageLoader(){
        // Create global configuration and initialize ImageLoader with this config
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true).build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).defaultDisplayImageOptions(defaultOptions).build();
        ImageLoader.getInstance().init(config);
    }

    private void callSearchFragment(){
        SearchFragment searchFragment = new SearchFragment();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragmentParentViewGroup, searchFragment);
        fragmentTransaction.addToBackStack(SearchFragment.TAG);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        int backCount = getFragmentManager().getBackStackEntryCount();
        if (backCount <= 1) {
            this.finish();
        } else {
            getFragmentManager().popBackStack();
        }
    }

}
