package com.apps.georgemarques.flightappbynintex.rest.service;

import com.apps.georgemarques.flightappbynintex.rest.model.FlightSearchData;

import java.util.Date;
import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

public interface ApiService {
    @GET("/api/Flight")
    public void getFlightsSearch(@Query("DepartureAirportCode") String departureAirportCode,
                                    @Query("ArrivalAirportCode") String arrivalAirportCode,
                                    @Query("DepartureDate") Date departureDate,
                                    @Query("ReturnDate") Date returnDate,
                                    Callback<List<FlightSearchData>> callback);
}
