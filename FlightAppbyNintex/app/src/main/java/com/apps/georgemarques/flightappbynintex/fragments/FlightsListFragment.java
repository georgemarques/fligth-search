package com.apps.georgemarques.flightappbynintex.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.apps.georgemarques.flightappbynintex.R;
import com.apps.georgemarques.flightappbynintex.adapter.FlightsAdapter;
import com.apps.georgemarques.flightappbynintex.model.FlightSearchInfo;
import com.apps.georgemarques.flightappbynintex.rest.RestClient;
import com.apps.georgemarques.flightappbynintex.rest.model.FlightSearchData;
import com.apps.georgemarques.flightappbynintex.util.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by george on 01/06/15.
 */
public class FlightsListFragment extends Fragment {
    @InjectView(R.id.recyclerViewFlights)
    RecyclerView recyclerViewFlights;

    @InjectView(R.id.progressBarLayout)
    LinearLayout progressBarLayout;

    private FlightSearchInfo fligthsInfo;
    private FlightsAdapter flightsAdapter;
    public static final String TAG = "FlightsListFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.flights_list_fragment,container, false);
        ButterKnife.inject(this, rootView);

        recyclerViewFlights.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(rootView.getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewFlights.setLayoutManager(layoutManager);

        fligthsInfo = getFligthsInfo(getArguments());

        getFlightsSearch(fligthsInfo.depAirportCode, fligthsInfo.retAirportCode, fligthsInfo.depatureDate, fligthsInfo.returnDate);
        return rootView;
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    private void getFlightsSearch(String depCode, String arrCode, String depDate, String retDate){
        progressBarLayout.setVisibility(View.VISIBLE);

        depDate = Utils.convertToRestDate(depDate);
        retDate = Utils.convertToRestDate(retDate);

        DateFormat df = new SimpleDateFormat(Utils.DATE_PATTERN);
        Date departureDate = new Date(0);
        Date returnDate = new Date(0);
        try{
            departureDate = df.parse(depDate);
            returnDate = df.parse(retDate);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        RestClient restClient = new RestClient();
        restClient.getApiService().getFlightsSearch(
                depCode,arrCode,
                departureDate,
                returnDate,
                flightsCallback
        );
    }

    private Callback<List<FlightSearchData>> flightsCallback = new Callback<List<FlightSearchData>>() {
        @Override
        public void success(List<FlightSearchData> flightSearchList, Response response) {
            flightsAdapter = new FlightsAdapter(flightSearchList);
            recyclerViewFlights.setAdapter(flightsAdapter);
            progressBarLayout.setVisibility(View.GONE);
        }

        @Override
        public void failure(RetrofitError error) {
            Toast.makeText(getActivity(), "Fail", Toast.LENGTH_SHORT).show();
            progressBarLayout.setVisibility(View.GONE);
        }
    };

    public FlightSearchInfo getFligthsInfo(Bundle bundle){
        FlightSearchInfo info =  (FlightSearchInfo) bundle.getParcelable(FlightSearchInfo.BUNDLE_KEY);
        return info;
    }
}
