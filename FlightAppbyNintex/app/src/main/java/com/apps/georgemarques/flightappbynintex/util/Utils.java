package com.apps.georgemarques.flightappbynintex.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by george on 28/05/15.
 */
public class Utils {
    public static String DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ssZ";

    public static boolean hasInternetConnectivity(Activity activity){
        ConnectivityManager connectivity = (ConnectivityManager)
                activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }
        }
        return false;

    }

    public static void showAlertDialog(String title, String message, Activity activity){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        // Showing Alert Message
        builder.show();
    }

    public static String getCurrentDate(){
        String date = "";
        Calendar c = Calendar.getInstance();

        date += String.valueOf(c.get(Calendar.DAY_OF_MONTH)) + "/";
        date += String.valueOf(c.get(Calendar.MONTH)) + "/";
        date += String.valueOf(c.get(Calendar.YEAR));

        return date;
    }

    public static boolean isGreaterDate(String date1, String date2){
        DateFormat df = new SimpleDateFormat(DATE_PATTERN);
        Date departureDate = new Date(0);
        Date returnDate = new Date(0);

        date1 = Utils.convertToRestDate(date1);
        date2 = Utils.convertToRestDate(date2);

        try{
            departureDate = df.parse(date1);
            returnDate = df.parse(date2);
        }
        catch (Exception e){
            return false;
        }

        if (departureDate.compareTo(returnDate) < 0){
            return true;
        }

        return false;
    }

    public static String convertToRestDate(String date){
        if(date.isEmpty()){
            return "";
        }

        //Formating date
        String part1 = date.substring(0, 22);
        String part2 = date.substring(22,24);

        return part1 + ":" + part2;
    }

    /**This class cannot be instantiated*/
    private Utils(){}
}
