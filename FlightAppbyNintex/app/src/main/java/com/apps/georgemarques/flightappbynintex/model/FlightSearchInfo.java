package com.apps.georgemarques.flightappbynintex.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by george on 02/06/15.
 */
public class FlightSearchInfo implements Parcelable{
    public static String BUNDLE_KEY = "FlightSearchInfo";

    public String depatureDate;
    public String returnDate;
    public String depAirportCode;
    public String retAirportCode;

    public FlightSearchInfo(String depatureDate, String returnDate, String depAirportCode, String retAirportCode) {
        this.depatureDate = depatureDate;
        this.returnDate = returnDate;
        this.depAirportCode = depAirportCode;
        this.retAirportCode = retAirportCode;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
