package com.apps.georgemarques.flightappbynintex.rest.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by george on 28/05/15.
 */
@Parcel
public class FlightSearchData {
    @SerializedName("AirlineLogoAddress")
    private String airlineLogoAddress;
    public String getAirlineLogoAddress(){
        return airlineLogoAddress;
    }

    @SerializedName("AirlineName")
    private String airlineName;
    public String getAirlineName(){
        return airlineName;
    }

    @SerializedName("InboundFlightsDuration")
    private String inboundFlightsDuration;
    public String getInboundFlightsDuration(){
        return inboundFlightsDuration;
    }

    @SerializedName("ItineraryId")
    private String itineraryId;

    @SerializedName("OutboundFlightsDuration")
    private String outboundFlightsDuration;
    public String getOutboundFlightsDuration(){
        return outboundFlightsDuration;
    }

    @SerializedName("Stops")
    private int stops;

    @SerializedName("TotalAmount")
    private float totalAmount;
    public float getTotalAmount(){
        return totalAmount;
    }

}
