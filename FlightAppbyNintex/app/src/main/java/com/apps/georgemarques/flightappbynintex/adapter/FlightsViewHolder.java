package com.apps.georgemarques.flightappbynintex.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.georgemarques.flightappbynintex.R;

/**
 * Created by george on 02/06/15.
 */
public class FlightsViewHolder extends RecyclerView.ViewHolder {
    protected ImageView airlineLogo;
    protected TextView airlineName;
    protected TextView outboundDuration;
    protected TextView inboundDuration;
    protected TextView totalAMount;

    public FlightsViewHolder(View v){
        super(v);
        this.airlineLogo = (ImageView) v.findViewById(R.id.airlineLogo);
        this.airlineName = (TextView) v.findViewById(R.id.airlineNameTv);
        this.inboundDuration = (TextView) v.findViewById(R.id.inboundDurTv);
        this.outboundDuration = (TextView) v.findViewById(R.id.outboundDurTv);
        this.totalAMount = (TextView) v.findViewById(R.id.amountTv);
    }
}
