package com.apps.georgemarques.flightappbynintex;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import java.util.Timer;
import java.util.TimerTask;


public class SplashScreenActivity extends Activity {
    Timer timer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        getActionBar().hide();
        timer = new Timer();

        // Shows SplashScreen for 3 seconds
        timer.schedule(new TimerTask() {
            public void run() {
                Intent startSearch = new Intent(SplashScreenActivity.this, SearchActivity.class);
                startActivity(startSearch);
            }
        }, 3000);
    }

}
